/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <string.h>

#include "amxb_rbus.h"
#include <amxb_rbus_version.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_path.h>

#define RBUS_CLI_MAX_PARAM      25

const char *getDataType_toString(rbusValueType_t type);

const char *getDataType_toString(rbusValueType_t type){
    const char* pTextData = "None";

    switch(type)
    {
        case RBUS_BOOLEAN:
             pTextData ="boolean";
             break;
        case RBUS_CHAR :
             pTextData ="char";
             break;
        case RBUS_BYTE:
             pTextData ="byte";
             break;
        case RBUS_INT8:
             pTextData ="int8";
             break;
        case RBUS_UINT8:
             pTextData ="uint8";
             break;
        case RBUS_INT16:
             pTextData ="int16";
             break;
        case RBUS_UINT16:
             pTextData ="uint16";
             break;
        case RBUS_INT32:
             pTextData ="int32";
             break;
        case RBUS_UINT32:
             pTextData = "uint32";
             break;
        case RBUS_INT64:
             pTextData = "int64";
             break;
        case RBUS_UINT64:
             pTextData = "uint64";
             break;
        case RBUS_STRING:
             pTextData = "string";
             break;
        case RBUS_DATETIME:
             pTextData = "datetime";
             break;
        case RBUS_BYTES:
             pTextData = "bytes";
             break;
        case RBUS_SINGLE:
             pTextData = "single";
             break;
        case RBUS_DOUBLE:
             pTextData = "double";
             break;
        case RBUS_PROPERTY:
        case RBUS_OBJECT:
        case RBUS_NONE:
             pTextData = "unknown";
             break;
    }
    return pTextData ;
}

int amxb_rbus_resolve(amxb_rbus_t* ctx,
                     const char* object_path,
                     const char* rel_path,
                     int32_t depth,
                     uint32_t flags,
                     bool* key_path,
                     amxc_var_t* resolved_objects) {
    int retval = AMXB_ERROR_INTERNAL;
    int i = 0;
    int index = 0;
    int numOfOutVals = 0;
    bool isWildCard = false;
    char* full_path= NULL; 
    const char *pInputParam[RBUS_CLI_MAX_PARAM] = {0, 0};

    rbusProperty_t outputVals = NULL;

    if((rel_path != NULL) && (*rel_path != 0)) {
          const size_t full_path_size = strnlen(object_path,RBUS_CLI_MAX_PARAM) + strnlen(rel_path,RBUS_CLI_MAX_PARAM);
          full_path = malloc(full_path_size + 1);
          if (!full_path) {
               goto exit;
          }
          snprintf(full_path,full_path_size+1,"%s%s",object_path,rel_path);
    }
    else{
         full_path=strndup(object_path,strnlen(object_path,RBUS_CLI_MAX_PARAM));
    }

    for(index = 0, i = 0; index < depth; index++, i++){
        pInputParam[index] = &full_path[i];
    }
    
    if(depth == 1)
    {
        
        if(pInputParam[0][strlen(pInputParam[0])-1] == '.' || strchr(pInputParam[0], '*')){
            isWildCard = true;
          }
    }

    if ((!isWildCard) && (1 == depth)){
          rbusValue_t getVal;
        
          retval = rbus_get(ctx->rbus_handle, pInputParam[0], &getVal);
          if(RBUS_ERROR_SUCCESS == retval){
               numOfOutVals = 1;
               rbusProperty_Init(&outputVals, pInputParam[0], getVal);
               rbusValue_Release(getVal);
           }
    }
    else{
        retval = rbus_getExt(ctx->rbus_handle, depth, pInputParam, &numOfOutVals, &outputVals);
    }

    if(RBUS_ERROR_SUCCESS == retval){
        rbusProperty_t next = outputVals;
        for (i = 0; i < numOfOutVals; i++)
        {
            rbusValue_t value = rbusProperty_GetValue(next);
            rbusValueType_t type = rbusValue_GetType(value);
            char *pStrVal = rbusValue_ToString(value,NULL,0);
            
            amxc_var_t *data =amxc_var_add_key(amxc_htable_t, resolved_objects, rbusProperty_GetName(next), NULL);
            amxc_var_add_key(cstring_t, data, "Type",getDataType_toString(type));
            amxc_var_add_key(cstring_t, data, "Value",pStrVal);

            //For printing the data from rbus
            printf ("Parameter %2d:\n\r", i+1);
            printf ("              Name  : %s\n\r", rbusProperty_GetName(next));
            printf ("              Type  : %s\n\r", getDataType_toString(type));
            printf ("              Value : %s\n\r", pStrVal);
            
            if(pStrVal){
                free(pStrVal);
            }
            next = rbusProperty_GetNext(next);
            when_null(next, exit);
        }
    }
    else
    {
        printf ("Line:%d Failed to get the data. Error : %d\n\r",__LINE__,retval);
    }
exit:
    /* Free the memory */
    free(full_path);
    rbusProperty_Release(outputVals);
    return retval;
}

int PRIVATE amxb_rbus_get(void* const ctx,
                               const char* object,
                               const char* search_path,
                               int32_t depth,
                               uint32_t access,
                               amxc_var_t* ret,
                               int timeout) { 
    bool key_path = false;
    int retval = -1;

    amxb_rbus_t* amxb_rbus_ctx = (amxb_rbus_t*) ctx;
    amxc_var_t* resolved_table = NULL;
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    resolved_table = amxc_var_add(amxc_htable_t, ret, NULL);
    
    when_null(object, exit);
    retval = amxb_rbus_resolve(amxb_rbus_ctx, object, search_path,depth, AMXB_FLAG_PARAMETERS, &key_path, resolved_table);
        
    if(retval != 0) {
        amxc_var_clean(ret);
        printf("Remote is not avilable\n");
        retval = AMXB_ERROR_BUS_NOT_FOUND;
        goto exit;
    }

exit:

    return retval;
}
