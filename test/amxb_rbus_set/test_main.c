/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "test_amxb_rbus_set.h"

int main(void) {
    int rv = 0;
    const struct CMUnitTest amx_tests[] = {
        cmocka_unit_test(test_rbus_set_index_path),
        cmocka_unit_test(test_rbus_set_key_path),
        cmocka_unit_test(test_rbus_unable_to_set_path),
    };

    rv = cmocka_run_group_tests(amx_tests, test_amxb_rbus_set_amx_setup, test_amxb_rbus_set_amx_teardown);
    return rv;
}
