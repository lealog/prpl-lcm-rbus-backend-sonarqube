/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>

#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include "amxb_rbus.h"
#include "test_amxb_rbus_invoke.h"

amxb_bus_ctx_t* bus_ctx = NULL;

//Buffer to hold the Object Name
char Object_name[512] = {0};

int test_amxb_rbus_invoke_amx_setup(UNUSED void** state) {

    system("amxrt -u rbus: -B ../mod-amxb-test-rbus.so --ODL 'import \"./function.so\" as func;' -A \"../test_data/test_nemo.odl\" &");

    assert_int_equal(amxb_be_load("../mod-amxb-test-rbus.so"), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "rbus:"), 0);
    return 0;
}

int test_amxb_rbus_invoke_amx_teardown(UNUSED void** state) {
    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);

    system("killall amxrt");

    amxb_be_remove_all();
    return 0;
}

void test_rbus_can_call_amx_function(UNUSED void** state) {
    amxc_var_t ret;
    amxb_invoke_t* invoke_ctx = NULL;

    amxc_var_init(&ret);
    assert_int_equal(amxb_new_invoke(&invoke_ctx, bus_ctx, "NeMo.Intf.1.Query.100.", NULL, "getResult"), AMXB_STATUS_OK);
    assert_int_equal(amxb_invoke(invoke_ctx, NULL, &ret, NULL, NULL, 5), AMXB_STATUS_OK);
    amxb_free_invoke(&invoke_ctx);
    amxc_var_clean(&ret);
}

static void test_rbus_invoke_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                UNUSED const amxc_var_t* const data,
                                UNUSED void* priv) {

}

static void test_rbus_invoke_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                     UNUSED amxb_request_t* req,
                                     UNUSED int status,
                                     UNUSED void* priv) {

}


amxd_status_t _test(UNUSED amxd_object_t* obj,UNUSED amxd_function_t* func,UNUSED amxc_var_t* args,UNUSED amxc_var_t* ret) {
return amxd_status_ok;
}

void test_rbus_invoke_amx_with_cb_function(UNUSED void** state) {
    amxc_var_t ret;
    amxb_invoke_t* invoke_ctx = NULL;

    amxc_var_init(&ret);

    assert_int_equal(amxb_new_invoke(&invoke_ctx, bus_ctx, "NeMo.Intf.1.Query.100.", NULL, "getResult"), AMXB_STATUS_OK);
    assert_int_equal(amxb_invoke(invoke_ctx, NULL, &ret, NULL, NULL, 5), AMXB_STATUS_OK);
    amxb_free_invoke(&invoke_ctx);

    amxc_var_clean(&ret);
}

void test_rbus_async_amx_invoke(UNUSED void** state) {
    amxb_invoke_t* invoke_ctx = NULL;
    amxb_request_t* req = NULL;
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args,AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxb_new_invoke(&invoke_ctx, bus_ctx, "NeMo.Intf.1.Query.100.", NULL, "getResult"), AMXB_STATUS_OK);
    assert_int_equal(amxb_async_invoke(invoke_ctx, &args, test_rbus_invoke_cb, test_rbus_invoke_done_cb, NULL, &req), AMXB_STATUS_OK);
    amxb_free_invoke(&invoke_ctx);
    amxb_close_request(&req);

    assert_int_equal(amxb_new_invoke(&invoke_ctx, bus_ctx, "NeMo.Intf.1.Query.100.", NULL, "getResult"), AMXB_STATUS_OK);
    assert_int_equal(amxb_async_invoke(invoke_ctx, &args, test_rbus_invoke_cb, test_rbus_invoke_done_cb, NULL, &req), AMXB_STATUS_OK);
    assert_int_equal(amxb_wait_for_request(req, 5), AMXB_STATUS_OK);
    amxb_free_invoke(&invoke_ctx);
    amxb_close_request(&req);
    amxc_var_clean(&args);
}


void test_rbus_call_non_existing_function(UNUSED void** state) {
    amxc_var_t ret;
    amxb_invoke_t* invoke_ctx = NULL;

    amxc_var_init(&ret);

    assert_int_equal(amxb_new_invoke(&invoke_ctx, bus_ctx, "NeMo.Intf.lan.", NULL, "dummy"), AMXB_STATUS_OK);
    assert_int_not_equal(amxb_invoke(invoke_ctx, NULL, &ret, NULL, NULL, 5), AMXB_STATUS_OK);
    amxb_free_invoke(&invoke_ctx);
    amxc_var_clean(&ret);
}

void test_rbus_call_non_existing_async_function(UNUSED void** state) {
    amxb_invoke_t* invoke_ctx = NULL;
    amxb_request_t* req = NULL;
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args,AMXC_VAR_ID_HTABLE);

    assert_int_equal(amxb_new_invoke(&invoke_ctx, bus_ctx, "NeMo.Intf.lan.", NULL, "dummy"), AMXB_STATUS_OK);
    assert_int_not_equal(amxb_async_invoke(invoke_ctx, &args, test_rbus_invoke_cb, test_rbus_invoke_done_cb, NULL, &req), AMXB_STATUS_OK);
    amxb_free_invoke(&invoke_ctx);
    amxb_close_request(&req);
    amxc_var_clean(&args);
}

void test_rbus_call_function_non_existing_path(UNUSED void** state) {
    amxc_var_t ret;
    amxb_invoke_t* invoke_ctx = NULL;

    amxc_var_init(&ret);

    assert_int_equal(amxb_new_invoke(&invoke_ctx, bus_ctx, "NeMo.Intf.XDSL.NonExisting.", NULL, "test"), AMXB_STATUS_OK);
    assert_int_not_equal(amxb_invoke(invoke_ctx, NULL, &ret, NULL, NULL, 5), AMXB_STATUS_OK);
    amxb_free_invoke(&invoke_ctx);
    amxc_var_clean(&ret);
}

void test_rbus_call_amx_using_index_path(UNUSED void** state) {
    amxc_var_t ret;
    amxb_invoke_t* invoke_ctx = NULL;
    const char* path = "NeMo.Intf.1.Query.100.";

    amxc_var_init(&ret);
    assert_int_equal(amxb_new_invoke(&invoke_ctx, bus_ctx, path, NULL, "getResult"), AMXB_STATUS_OK);
    assert_int_equal(amxb_invoke(invoke_ctx, NULL, &ret, NULL, NULL, 5), AMXB_STATUS_OK);
    amxb_free_invoke(&invoke_ctx);

    amxc_var_clean(&ret);
}

void test_rbus_call_amx_using_key_path(UNUSED void** state) {
    amxc_var_t ret;
    amxb_invoke_t* invoke_ctx = NULL;

    amxc_var_init(&ret);

    assert_int_equal(amxb_new_invoke(&invoke_ctx, bus_ctx, "NeMo.Intf.[lan].Query.[SomeKey].", NULL, "getResult"), AMXB_STATUS_OK);
    assert_int_equal(amxb_invoke(invoke_ctx, NULL, &ret, NULL, NULL, 5), AMXB_STATUS_OK);
    amxb_free_invoke(&invoke_ctx);

    amxc_var_clean(&ret);
}

void test_rbus_invoke_amx_with_cb_function_on_non_existing_path(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxb_invoke_t* invoke_ctx = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_int_equal(amxb_new_invoke(&invoke_ctx, bus_ctx, "Does.Not.Exist.", NULL, "test"), AMXB_STATUS_OK);
    assert_int_not_equal(amxb_invoke(invoke_ctx, &args, &ret, test_rbus_invoke_cb, NULL, 5), AMXB_STATUS_OK);
    amxb_free_invoke(&invoke_ctx);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}


void test_rbus_can_call_amx_function_with_arguments(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;
    amxb_invoke_t* invoke_ctx = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args,AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t,&args,"input",5);
    assert_int_equal(amxb_new_invoke(&invoke_ctx, bus_ctx, "NeMo.Intf.1.Query.100.", NULL, "getResult"), AMXB_STATUS_OK);
    assert_int_equal(amxb_invoke(invoke_ctx, &args, &ret, NULL, NULL, 5), AMXB_STATUS_OK);
    amxb_free_invoke(&invoke_ctx);
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}