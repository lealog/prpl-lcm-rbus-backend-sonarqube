<!--
SPDX-License-Identifier: BSD-2-Clause-Patent
Copyright (c) 2022 the amxb_rbus contributors
This code is subject to the terms of the BSD+Patent license.
See LICENSE file for more details.
-->
The amxb_rbus project is copyright 2022 its contributors.
This file lists all contributors.

Companies and organizations:
This lists companies/organizations that sponsored development on amxb_rbus,
rather than getting paid for implementing it
- Vodafone
- Consult RED
- GlobalLogic

Individuals:

- Sumit Kumar Suman (Vodafone)
- Shaji Matthew (Vodafone)
- Panchakshari HS (Vodafone)
- Saran VS (Vodafone)
- Oscar Leal (Vodafone)

- Oscar Carter (Consult RED)
- Dave Chapman (Consult RED)

- Oleksandr Remez (GlobalLogic)
- Mariusz Dudek (GlobalLogic)

In addition, some parts were copied from other projects.

- Parts of amxb_ubus (https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_ubus.git) created by SoftAtHome
