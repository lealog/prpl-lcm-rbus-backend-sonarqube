/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#ifndef __TEST_AMXB_RBUS_CONNECT_H__
#define __TEST_AMXB_RBUS_CONNECT_H__

void test_amxb_rbus_connect_disconnect(void** state);
void test_amxb_rbus_get_fd(void** state);
void test_amxb_info(void** state);

#endif // __TEST_AMXB_RBUS_CONNECT_H__
