/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <signal.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>

#include <amxb/amxb.h>

#include "amxb_rbus.h"
#include "test_amxb_rbus_set.h"

static amxb_bus_ctx_t* bus_ctx = NULL;

int test_amxb_rbus_set_amx_setup(UNUSED void** state) {
    system("amxrt -u rbus: -B ../mod-amxb-test-rbus.so -A ../test_data/test_nemo.odl &");

    assert_int_equal(amxb_be_load("../mod-amxb-test-rbus.so"), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "rbus:"), 0);

    sleep(1);

    return 0;
}

int test_amxb_rbus_set_amx_teardown(UNUSED void** state) {
    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);

    system("killall amxrt");

    amxb_be_remove_all();

    return 0;
}

void test_rbus_set_index_path(UNUSED void** state) {
    amxc_var_t values;
    amxc_var_t ret;
    amxc_var_t* results = NULL;
    const char* path = "NeMo.Intf.1.Query.100.";
    amxc_var_init(&values);
    amxc_var_init(&ret);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Description", "change1"); //Create variant values with leaf name and its new value.
    assert_int_equal(amxb_set(bus_ctx, path, &values, &ret, DEFAULT_TIMEOUT), 0); //passed values to set to the leaf.

    amxc_var_clean(&values);
    amxc_var_init(&values);
    assert_int_equal(amxb_get(bus_ctx, path, DEFAULT_DEPTH, &values, DEFAULT_TIMEOUT), 0);
    amxc_var_dump(&values, STDOUT_FILENO);
    assert_int_equal(amxc_var_type_of(&values), AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(&values.data.vl), 1);
   
    results = GETI_ARG(&values, 0);
    assert_non_null(results);
    assert_int_equal(amxc_var_type_of(results), AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxc_htable_size(&results->data.vm), 3);
    results = GET_ARG(results, "NeMo.Intf.1.Query.100.Description"); //Found the target leaf
    assert_non_null(results);
    assert_int_equal(amxc_var_type_of(results), AMXC_VAR_ID_HTABLE); //Using that target leaf get htable of target leaf
    results = GET_ARG(results, "Value");
    assert_non_null(results);
    const char *str_name = amxc_var_constcast(cstring_t, results);
    assert_string_equal("change1",str_name); //Found the new value of  target leaf.

    amxc_var_clean(&values);
    amxc_var_clean(&ret);
}

void test_rbus_set_key_path(UNUSED void** state) {
    amxc_var_t values;
    amxc_var_t ret;
    amxc_var_t* results = NULL;
    const char* path = "NeMo.Intf.1.";
    const char* ipath = "NeMo.Intf.1.Query.100.ResultString";
    amxc_var_init(&values);
    amxc_var_init(&ret);
    
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &values, "Enable", false);
    assert_int_equal(amxb_set(bus_ctx, path, &values, &ret, DEFAULT_TIMEOUT), 0);
   
    amxc_var_clean(&values);
    amxc_var_init(&values);
    assert_int_equal(amxb_get(bus_ctx, path, DEFAULT_DEPTH, &values, DEFAULT_TIMEOUT), 0);
    amxc_var_dump(&values, STDOUT_FILENO);
    assert_int_equal(amxc_var_type_of(&values), AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(&values.data.vl), 1);
    results = GETI_ARG(&values, 0);
    assert_non_null(results);
    assert_int_equal(amxc_var_type_of(results), AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxc_htable_size(&results->data.vm), 7);
    results = GET_ARG(results, "NeMo.Intf.1.Enable"); //Found the target leaf
    assert_non_null(results);
    assert_int_equal(amxc_var_type_of(results), AMXC_VAR_ID_HTABLE); //Using that target leaf get htable of target leaf
    results = GET_ARG(results, "Value");
    assert_non_null(results);
    char *str_name = amxc_var_dyncast(cstring_t, results);
    if (strlen(str_name) < 5) { str_name = realloc(str_name,6); } //make sure both  true and false will fit in the char*
    strncmp("0",str_name,10) ? sprintf(str_name,"true"):sprintf(str_name,"false");
    assert_string_equal("false",str_name); //Found the new value of  target leaf.
    results = GETI_ARG(&values, 0); //To verify the existing component data is not get impacted changes.
    results = GET_ARG(results, ipath); //To verify the existing component data is not get impacted changes.
    amxc_var_dump(results, STDOUT_FILENO);
    assert_non_null(results);      //It should have at least key value of "ipath"/"NeMo.Intf.1.Query.100.ResultString".
    
    free(str_name);
    amxc_var_clean(&results);
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
}

void test_rbus_unable_to_set_path(UNUSED void** state) {
    amxc_var_t values;
    amxc_var_t ret;
    amxc_var_t* results = NULL;
    const char* path = "NeMo.Intf.lan.";
    amxc_var_init(&values);
    amxc_var_init(&ret);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Description", "change3");
    assert_int_equal(amxb_set(bus_ctx, path, &values, &ret, DEFAULT_TIMEOUT), AMXB_ERROR_BUS_NOT_FOUND); // Target leaf not found in component to set new value 

    amxc_var_clean(&values);
    amxc_var_clean(&ret);
}

