/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <signal.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>

#include <amxb/amxb.h>


#include "amxb_rbus.h"
#include "test_amxb_rbus_list.h"

amxb_bus_ctx_t* bus_ctx = NULL;

//Buffer to hold the Object Name
char Object_name[512] = {0};

int test_amxb_rbus_list_amx_setup(UNUSED void** state) {
    amxc_string_t txt;
    char buff[sizeof(Object_name)] = {0};  
 
    amxc_string_init(&txt, 0);

    amxc_string_reset(&txt);
    amxc_string_setf(&txt, "amxrt -u rbus: -B ../mod-amxb-test-rbus.so -A ../test_data/test_nemo.odl &");
    system(amxc_string_get(&txt, 0));

    amxc_string_clean(&txt);

    assert_int_equal(amxb_be_load("../mod-amxb-test-rbus.so"), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "rbus:"), 0);
    
    system("rbuscli discallcomponents | grep \"Component 2:\" | cut -d \":\" -f 2 > /tmp/Compname.txt");

    FILE *fp = fopen("/tmp/Compname.txt","r");
    if(fp != NULL)
    {
        if ( fscanf(fp, "%s", buff) != NULL )
        {
            printf("Component is: %s\n\r", buff);
            strncpy(Object_name, buff, sizeof(buff));
        }

    }
    else
    {
        printf("File Open Failed\n\r");
        exit(-1);
    }

    fclose(fp);
   
    sleep(1);

    return 0;
}

int test_amxb_rbus_list_amx_teardown(UNUSED void** state) {
    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);

    system("killall amxrt");

    amxb_be_remove_all();

    return 0;
}


static void test_list_cb( amxb_bus_ctx_t* ctx,
                          UNUSED amxc_var_t* const data,
                          void* priv ) {
    amxb_request_t* recvd_data = (amxb_request_t*)priv;
    char** output_data = NULL;
    int index = 0;

    if(recvd_data->bus_retval)
    {
         output_data =(char**)recvd_data->bus_data;

         for (index = 0; index < recvd_data->bus_retval; index++)
            {
                printf ("\tElement %d: %s\n\r", (index + 1), output_data[index]);
                free(output_data[index]);
            }
            free(recvd_data->bus_data);
            recvd_data->bus_data = NULL;
    }
    else
    {
       printf("No elements discovered!\n\r");
    } 

   amxb_close_request(&recvd_data);
} 

void test_rbus_can_list_amx_valid_object(UNUSED void** state) {
    void *priv = NULL;
    int all_flags = AMXB_FLAG_FUNCTIONS | AMXB_FLAG_PARAMETERS | AMXB_FLAG_OBJECTS | AMXB_FLAG_INSTANCES;
    assert_int_equal(amxb_list(bus_ctx, Object_name, all_flags, test_list_cb, priv), AMXB_STATUS_OK);
}

//The below Test Case will Pass but valgrind will report a memory leak issue due to a bug 
// in amxb_list() implementation. Will be raising a defect ticket in Prpl Jira.
void test_rbus_can_list_amx_empty_object(UNUSED void** state) {
    void *priv = NULL;
    int all_flags = AMXB_FLAG_FUNCTIONS | AMXB_FLAG_PARAMETERS | AMXB_FLAG_OBJECTS | AMXB_FLAG_INSTANCES;
    assert_int_not_equal(amxb_list(bus_ctx, NULL, all_flags,test_list_cb, priv), AMXB_STATUS_OK);
} 

void test_rbus_can_list_amx_invalid_object(UNUSED void** state) {
    void *priv = NULL;
    int all_flags = AMXB_FLAG_FUNCTIONS | AMXB_FLAG_PARAMETERS | AMXB_FLAG_OBJECTS | AMXB_FLAG_INSTANCES;
    strncpy(Object_name, "amx_test",sizeof(Object_name));
    assert_int_equal(amxb_list(bus_ctx, Object_name, all_flags, test_list_cb, priv), AMXB_STATUS_OK);
}

