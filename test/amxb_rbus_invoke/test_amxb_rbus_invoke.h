/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#ifndef __TEST_AMXB_RBUS_INVOKE_H__
#define __TEST_AMXB_RBUS_INVOKE_H__

#include <stdlib.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc_variant_type.h>
#include <amxc/amxc_lqueue.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_types.h>


int test_amxb_rbus_invoke_amx_setup(void** state);
int test_amxb_rbus_invoke_amx_teardown(void** state);
void test_rbus_can_call_amx_function(void** state);
void test_rbus_can_call_amx_function_with_arguments(void** state);
void test_rbus_invoke_amx_with_cb_function(void** state);
void test_rbus_async_amx_invoke(void** state);
void test_rbus_call_non_existing_function(void** state);
void test_rbus_call_non_existing_async_function(void** state);
void test_rbus_call_function_non_existing_path(void** state);
void test_rbus_call_amx_using_index_path(void** state);
void test_rbus_call_amx_using_key_path(void** state);
void test_rbus_invoke_amx_with_cb_function_on_non_existing_path(void** state);

// Implementation for functions in test_nemo.odl
// Does nothing, but is needed so we can call a function.
amxd_status_t _test(UNUSED amxd_object_t* obj,UNUSED amxd_function_t* func,amxc_var_t* args,UNUSED amxc_var_t* ret);

#endif // __TEST_AMXB_RBUS_INVOKE_H__