/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "test_amxb_rbus_list.h"

int main() {
        const struct CMUnitTest tests[] = {
          cmocka_unit_test(test_rbus_can_list_amx_valid_object),
          cmocka_unit_test(test_rbus_can_list_amx_empty_object),
          cmocka_unit_test(test_rbus_can_list_amx_invalid_object),
    };

    return cmocka_run_group_tests(tests, test_amxb_rbus_list_amx_setup, test_amxb_rbus_list_amx_teardown);
}
