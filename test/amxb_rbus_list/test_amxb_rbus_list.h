/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#ifndef __TEST_AMXB_RBUS_LIST_H__
#define __TEST_AMXB_RBUS_LIST_H__

#include <stdlib.h>
#include <amxc/amxc_macros.h>

int test_amxb_rbus_list_amx_setup(UNUSED void** state);
int test_amxb_rbus_list_amx_teardown(UNUSED void** state);
void test_rbus_can_list_amx_valid_object(UNUSED void** state);
void test_rbus_can_list_amx_empty_object(UNUSED void** state);
void test_rbus_can_list_amx_invalid_object(UNUSED void** state);
  
#endif // __TEST_AMXB_RBUS_LIST_H__
