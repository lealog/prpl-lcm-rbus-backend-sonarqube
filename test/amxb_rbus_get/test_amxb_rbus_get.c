/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <signal.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>

#include <amxb/amxb.h>
#include <rbus.h>

#include "amxb_rbus.h"
#include "test_amxb_rbus_get.h"

static amxb_bus_ctx_t* bus_ctx = NULL;

int test_amxb_rbus_get_amx_setup(UNUSED void** state) {
    system("amxrt -u rbus: -B ../mod-amxb-test-rbus.so -A ../test_data/test_nemo.odl -A ../test_data/test_full_types.odl &");

    assert_int_equal(amxb_be_load("../mod-amxb-test-rbus.so"), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "rbus:"), 0);

    sleep(1);

    return 0;
}

int test_amxb_rbus_get_amx_teardown(UNUSED void** state) {
    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);

    system("killall amxrt");

    amxb_be_remove_all();

    return 0;
}

void test_rbus_get_not_wildcard_path(UNUSED void** state) {
    amxc_var_t values;
    amxc_var_t* results = NULL;
    const char* path = "NeMo.Intf.3.Name";
    amxc_var_init(&values);

    assert_int_equal(amxb_get(bus_ctx, path, DEFAULT_DEPTH, &values, DEFAULT_TIMEOUT), 0);
    amxc_var_dump(&values, STDOUT_FILENO);
    assert_int_equal(amxc_var_type_of(&values), AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(&values.data.vl), 1); //size of list should be 1 as only one htable is stored.
    
    results = GETI_ARG(&values, 0);
    assert_non_null(results); //By default also at least key should present value my be null
    assert_int_equal(amxc_var_type_of(results), AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxc_htable_size(&results->data.vm), 1);  //size of htable should be 1 as only one htable is stored.
    results = GET_ARG(results, path);
    assert_non_null(results); //By default also at least key should present value my be null

    amxc_var_clean(&values);
    
}

void test_rbus_get_wildcard_path(UNUSED void** state) {
    amxc_var_t values;
    amxc_var_t* results = NULL;
    const char* path = "NeMo.Intf.1.Query.100.";
    amxc_var_init(&values);

    assert_int_equal(amxb_get(bus_ctx, path, DEFAULT_DEPTH, &values, DEFAULT_TIMEOUT), 0);
    amxc_var_dump(&values, STDOUT_FILENO);
    assert_int_equal(amxc_var_type_of(&values), AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(&values.data.vl), 1); //size of list should be 1 as only one htable is stored.
    results = GETI_ARG(&values, 0);
    assert_non_null(results);
   
    assert_int_equal(amxc_var_type_of(results), AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxc_htable_size(&results->data.vm), 3); //size of htable should be 3 composite vairant is stored.
    assert_int_equal(amxc_var_type_of(results), AMXC_VAR_ID_HTABLE);
    assert_non_null(GET_ARG(results, "NeMo.Intf.1.Query.100.Description")); //Stored element is found with key name
    assert_non_null(GET_ARG(results, "NeMo.Intf.1.Query.100.Subscribers"));
    assert_non_null(GET_ARG(results, "NeMo.Intf.1.Query.100.ResultString"));
   
    amxc_var_clean(&values);

}

void test_rbus_get_multilevel_path(UNUSED void** state){
    amxc_var_t values;
    amxc_var_t* results = NULL;
    const char* paths[] = {
        "NeMo."
    };
    amxc_var_init(&values);

    assert_int_equal(amxb_get(bus_ctx, paths[0], DEFAULT_DEPTH, &values, DEFAULT_TIMEOUT), AMXB_STATUS_OK);
    amxc_var_dump(&values, STDOUT_FILENO);
    assert_int_equal(amxc_var_type_of(&values), AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(&values.data.vl), 1); //size of list should be 1 as only one htable is stored.
    results = GETI_ARG(&values, 0);
    assert_non_null(results);
    assert_int_equal(amxc_var_type_of(results), AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxc_htable_size(&results->data.vm), 30); //size of htable should be composite vairant is stored.
    
    amxc_var_clean(&values);
}
void test_rbus_unable_to_get(UNUSED void** state){
    amxc_var_t values;
    amxc_var_t* results = NULL;
    const char* paths[] = {
        "NeMo.Intf.lan.Query.100."
    };
    amxc_var_init(&values);
 
    //object not found "A negative scenario"
    assert_int_equal(amxb_get(bus_ctx, paths[3], DEFAULT_DEPTH, &values, DEFAULT_TIMEOUT), AMXB_ERROR_BUS_NOT_FOUND);
    results = GETI_ARG(&values, 0);
    assert_null(results);
    
    amxc_var_clean(&values);
}

#define NUM_PARAM_TYPES 11
void test_rbus_get_types(UNUSED void** state) {
    amxc_var_t values;
    amxc_var_t* results = NULL;
    const char paths [NUM_PARAM_TYPES][sizeof("TestObject.param11")] = {
        "TestObject.param1",
        "TestObject.param2",
        "TestObject.param3",
        "TestObject.param4",
        "TestObject.param5",
        "TestObject.param6",
        "TestObject.param7",
        "TestObject.param8",
        "TestObject.param9",
        "TestObject.param10",
        "TestObject.param11"
    };
    amxc_var_init(&values);
    for (int i =0 ; i < NUM_PARAM_TYPES; i++) {
        assert_int_equal(amxb_get(bus_ctx, paths[i], DEFAULT_DEPTH, &values, DEFAULT_TIMEOUT), 0);

        assert_int_equal(amxc_var_type_of(&values), AMXC_VAR_ID_LIST);
        assert_int_equal(amxc_llist_size(&values.data.vl), 1); //size of list should be 1 as only one htable is stored.
        results = GETI_ARG(&values, 0);
        assert_non_null(results);
    }
   
    amxc_var_clean(&values);

}
