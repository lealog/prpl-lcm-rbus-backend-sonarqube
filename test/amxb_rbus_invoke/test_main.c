/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "test_amxb_rbus_invoke.h"

int main() {
        const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_rbus_can_call_amx_function),
        cmocka_unit_test(test_rbus_invoke_amx_with_cb_function),
        cmocka_unit_test(test_rbus_async_amx_invoke),
        cmocka_unit_test(test_rbus_call_non_existing_function),
        cmocka_unit_test(test_rbus_call_function_non_existing_path),
        cmocka_unit_test(test_rbus_call_non_existing_async_function),
        cmocka_unit_test(test_rbus_call_amx_using_index_path),
        cmocka_unit_test(test_rbus_call_amx_using_key_path),
        cmocka_unit_test(test_rbus_invoke_amx_with_cb_function_on_non_existing_path),
        cmocka_unit_test(test_rbus_can_call_amx_function_with_arguments)
    };

    return cmocka_run_group_tests(tests, test_amxb_rbus_invoke_amx_setup, test_amxb_rbus_invoke_amx_teardown);
}
