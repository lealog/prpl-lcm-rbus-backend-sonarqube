/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#ifndef __TEST_AMXB_RBUS_GET_H__
#define __TEST_AMXB_RBUS_GET_H__

#include <amxc/amxc_macros.h>
int test_amxb_rbus_get_amx_setup(UNUSED void** state);
int test_amxb_rbus_get_amx_teardown(UNUSED void** state);
void test_rbus_get_not_wildcard_path(UNUSED void** state);
void test_rbus_get_wildcard_path(UNUSED void** state);
void test_rbus_get_multilevel_path(UNUSED void** state);
void test_rbus_unable_to_get(UNUSED void** state);
void test_rbus_get_types(UNUSED void** state);
#endif // __TEST_AMXB_RBUS_GET_H__
