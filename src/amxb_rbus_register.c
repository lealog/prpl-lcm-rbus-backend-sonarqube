/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "amxb_rbus.h"
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

//According to TR-069 maximal path (e.g. ParameterValueStruct) length is 256
#define MAX_PATH_LEN 257

amxb_rbus_t* g_amxb_rbus_ctx;


static rbusError_t amxb_error_convert(amxd_status_t error) {
    switch(error) {
    case amxd_status_ok:
        return RBUS_ERROR_SUCCESS;
    case amxd_status_object_not_found:
    case amxd_status_function_not_found:
    case amxd_status_parameter_not_found:
        return RBUS_ERROR_DESTINATION_NOT_FOUND;
    case amxd_status_function_not_implemented:
    case amxd_status_invalid_function:
        return RBUS_ERROR_INVALID_METHOD;
    case amxd_status_invalid_function_argument:
    case amxd_status_invalid_arg:
    case amxd_status_invalid_type:
    case amxd_status_invalid_attr:
    case amxd_status_invalid_value:
    case amxd_status_file_not_found:
    case amxd_status_invalid_path:
    case amxd_status_invalid_expr:
        return RBUS_ERROR_INVALID_INPUT;
    case amxd_status_invalid_name:
    case amxd_status_missing_key:
        return RBUS_ERROR_ELEMENT_NAME_MISSING;
    case amxd_status_invalid_action:
        return RBUS_ERROR_INVALID_OPERATION;
    case amxd_status_duplicate:
        return RBUS_ERROR_ELEMENT_NAME_DUPLICATE;
    case amxd_status_out_of_mem:
    case amxd_status_recursion:
        return RBUS_ERROR_OUT_OF_RESOURCES;
    case amxd_status_deferred:
        return RBUS_ERROR_ASYNC_RESPONSE;
    case amxd_status_read_only:
    case amxd_status_permission_denied:
        return RBUS_ERROR_ACCESS_NOT_ALLOWED;
    default:
        return RBUS_ERROR_DESTINATION_RESPONSE_FAILURE;
    }
}

static rbusError_t amxb_rbus_invoke_function(const char* obj_name,
                                             const char* func_name,
                                             amxc_var_t* args,
                                             amxc_var_t* ret) {
    amxd_status_t retval = RBUS_ERROR_SUCCESS;
    amxb_rbus_t* amxb_rbus_ctx = g_amxb_rbus_ctx;
    amxd_object_t* root_obj = amxd_dm_get_root(amxb_rbus_ctx->dm);
    amxd_object_t* obj = amxd_object_findf(root_obj, "%s", obj_name);

    //TODO: Investigate possibility to set 'access' basing on rbus
    //property/options argument. Hardcoding default vaulue for now.
    amxc_var_add_key(uint32_t, args, "access", 0);

    retval = amxd_object_invoke_function(obj, func_name, args, ret);

    switch(retval) {
    case amxd_status_deferred: //nothing extra to do on deferred
    case amxd_status_ok:
        break;
    default:
        fprintf(stderr,"Error during the %s execution for %s, rc: %d\n",
               func_name,
               obj_name,
               retval);
        break;
    }

    return amxb_error_convert(retval);
}

static void amxb_rbus_split_path(const char* path,
                                 char** obj_path,
                                 char** rel_path) {
    char *rel = NULL;
    static char obj_name[MAX_PATH_LEN];
    static char rel_name[MAX_PATH_LEN];

    snprintf(obj_name, MAX_PATH_LEN, "%s", path);
    rel = strrchr(obj_name, '.') + 1;
    snprintf(rel_name, MAX_PATH_LEN, "%s", rel);
    *rel = '\0';
    *obj_path = obj_name;
    *rel_path = rel_name;
}

#define AMX_GET_FUNC "_get"
static rbusError_t amxb_rbus_get_handler(rbusHandle_t handle,
                                         rbusProperty_t property,
                                         rbusGetHandlerOptions_t* options) {
    rbusError_t rc = RBUS_ERROR_SUCCESS;
    amxc_var_t args;
    amxc_var_t ret;
    rbusValue_t value = NULL;
    char* rel_path = NULL;
    char* obj_path = NULL;

    amxb_rbus_split_path(rbusProperty_GetName(property), &obj_path, &rel_path);

    //TODO: Investigate possibility to set 'depth' basing on rbus
    //property/options argument. Hardcoding default vaulue for now.
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "rel_path", rel_path);
    amxc_var_add_key(uint32_t, &args, "depth", INT32_MAX);

    rc = amxb_rbus_invoke_function(obj_path, AMX_GET_FUNC, &args, &ret);
    when_false(rc == RBUS_ERROR_SUCCESS, exit);

    rbusValue_Init(&value);
    if(amxc_var_type_of(&ret) == AMXC_VAR_ID_HTABLE) {
        const amxc_htable_t* table = amxc_var_constcast(amxc_htable_t, &ret);

        amxc_htable_for_each(it, table) {
            const char* key = amxc_htable_it_get_key(it);
            amxc_var_t* val = amxc_var_from_htable_it(it);

            if(!strcmp(key, obj_path)) {
                amxb_rbus_set_retval(value, val, rel_path);
            }
        }
    }
    else {
        amxb_rbus_set_retval(value, &ret, rel_path);
    }
    rbusProperty_SetValue(property, value);
    rbusValue_Release(value);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return rc;
}

#define AMX_SET_FUNC "_set"
static rbusError_t amxb_rbus_set_handler(rbusHandle_t handle,
                                         rbusProperty_t property,
                                         rbusSetHandlerOptions_t* opts) {
    rbusError_t rc = RBUS_ERROR_SUCCESS;
    char* rel_path = NULL;
    char* obj_path = NULL;
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_t* params = NULL;

    amxb_rbus_split_path(rbusProperty_GetName(property), &obj_path, &rel_path);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxb_rbus_set_value(params, rbusProperty_GetValue(property), rel_path);

    rc = amxb_rbus_invoke_function(obj_path, AMX_SET_FUNC, &args, &ret);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return rc;
}

inline static void amxb_rbus_free_obj(amxb_rbus_object_t* obj) {
    if (!obj) return;
    free(obj->rbus_obj.name);
    free(obj);
}

void PRIVATE amxb_rbus_obj_it_free(amxc_llist_it_t* it) {
    if (!it) return;
    amxb_rbus_free_obj(amxc_llist_it_get_data(it, amxb_rbus_object_t, it));
}

static bool amxb_rbus_is_obj_registered(amxb_rbus_t* ctx,
                                        const char* obj_path) {

    amxc_llist_for_each(it, (&ctx->registered_objs)) {
        amxb_rbus_object_t* obj = amxc_llist_it_get_data(it,
                                                         amxb_rbus_object_t,
                                                         it);
        if(strcmp(obj->rbus_obj.name, obj_path) == 0) {
            return true;
        }
    }
    return false;
}

struct rbus_function_data {
    char * methodName;
    amxb_request_t* request;
    rbusObject_t inParams;
    rbusMethodAsyncHandle_t asyncHandle;
};

static void rbus_method_input_args_translate(rbusObject_t inParams, amxc_var_t * args) {
    rbusProperty_t property = rbusObject_GetProperties(inParams);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    while (property) {
        amxb_rbus_set_value(args,
                            rbusProperty_GetValue(property),
                            rbusProperty_GetName(property));
        property = rbusProperty_GetNext(property);
    }
}

static void * async_method_handler_thread(void * ptr) {
    amxc_var_t ret;
    amxc_var_t args;
    char* bracket = NULL;
    char* obj_path = NULL;
    char* method_name = NULL;
    rbusError_t rc = RBUS_ERROR_BUS_ERROR;
    rbusValue_t value = NULL;
    rbusObject_t outParams = NULL;

    struct rbus_function_data * function_data = (struct rbus_function_data *)ptr;

    amxb_rbus_split_path(function_data->methodName, &obj_path, &method_name);
    /* Remove brackets */
    if((bracket = strrchr(method_name, '('))) {
        *bracket = '\0';
    }

    amxc_var_init(&ret);

    rbusProperty_t callback = rbusObject_GetProperty(function_data->inParams,"__callbackPtr");
    if (callback != NULL) {
        #if INTPTR_MAX == INT64_MAX
        function_data->request = (amxb_request_t*) rbusValue_GetUInt64(rbusProperty_GetValue(callback));
        #elif INTPTR_MAX == INT32_MAX
        function_data->request = (amxb_request_t*) rbusValue_GetUInt32(rbusProperty_GetValue(callback));
        #endif
    }

    rbus_method_input_args_translate(function_data->inParams,&args);
    rc = amxb_rbus_invoke_function(obj_path, method_name, &args, &ret);
    when_false(rc == RBUS_ERROR_SUCCESS, exit);

    if (!rbusValue_Init(&value)) {
        fprintf(stderr,"init rbusValue failed\n");
        goto exit;
    }
    if (!rbusObject_Init(&outParams,NULL)) {
        fprintf(stderr,"init rbusValue failed\n");
        rbusValue_Release(value);
        goto exit;
    }
    amxb_rbus_set_retval(value, &ret, NULL);
    rbusObject_SetValue(outParams, "value", value);
    rbusValue_Release(value);

    if (callback != NULL) {
        if (!rbusValue_Init(&value)) {
            fprintf(stderr,"init rbusValue failed\n");
            goto exit;
        }
        rbusValue_SetUInt64(value,(uintptr_t)function_data->request);
        rbusObject_SetValue(outParams, "__callbackPtr", value);
        rbusValue_Release(value);
    }

    rc = rbusMethod_SendAsyncResponse(function_data->asyncHandle, rc, outParams);
    if(rc != RBUS_ERROR_SUCCESS) {
        fprintf(stderr,"%s SendAsyncResponse rc:%i\n", function_data->methodName, rc);
    }

    rbusObject_Release(outParams);
exit:
    rbusObject_Release(function_data->inParams);
    free(function_data->methodName);
    free(function_data);
    amxc_var_clean(&ret);
    return NULL;
}

static rbusError_t amxb_rbus_method_handler(rbusHandle_t handle,
                                            char const* methodName,
                                            rbusObject_t inParams,
                                            rbusObject_t outParams,
                                            rbusMethodAsyncHandle_t asyncHandle) {
    amxc_var_t ret;
    amxc_var_t args;
    char* bracket = NULL;
    char* obj_path = NULL;
    char* method_name = NULL;
    rbusError_t rc = RBUS_ERROR_BUS_ERROR;
    rbusValue_t value = NULL;

    if (!asyncHandle) {
        amxb_rbus_split_path(methodName, &obj_path, &method_name);
        /* Remove brackets */
        if((bracket = strrchr(method_name, '('))) {
            *bracket = '\0';
        }

        amxc_var_init(&ret);
        rbus_method_input_args_translate(inParams,&args);
        rc = amxb_rbus_invoke_function(obj_path, method_name, &args, &ret);
        when_false(rc == RBUS_ERROR_SUCCESS, exit);

        rbusValue_Init(&value);  
        amxb_rbus_set_retval(value, &ret, NULL);
        rbusObject_SetValue(outParams, "value", value);
        rbusValue_Release(value); 
        amxc_var_clean(&ret);
    } else {
        pthread_t pid;
        struct rbus_function_data * data = malloc(sizeof(struct rbus_function_data));
        if (!data) {
            fprintf(stderr,"malloc failed\n");
            goto exit;
        }
        data->methodName = strdup(methodName);
        data->inParams = inParams;
        data->asyncHandle = asyncHandle;

        rbusObject_Retain(inParams);
        pthread_create(&pid, NULL, async_method_handler_thread, data);
        pthread_detach(pid);
        rc = RBUS_ERROR_ASYNC_RESPONSE;
        rbusValue_Init(&value);
        rbusObject_SetValue(outParams, "value", value);
        rbusValue_Release(value); 
    }
exit:
    return rc;
}

static rbusError_t amx_rbus_regiser_obj(amxb_rbus_object_t* obj,
                                 amxb_rbus_t* amxb_rbus_ctx) {
    bool is_dup = amxb_rbus_is_obj_registered(amxb_rbus_ctx,
                                              obj->rbus_obj.name);                           
    rbusError_t rc = is_dup ? RBUS_ERROR_ELEMENT_NAME_DUPLICATE :
        rbus_regDataElements(amxb_rbus_ctx->rbus_handle,
                             1,
                             &obj->rbus_obj);
                             
    if(rc != RBUS_ERROR_SUCCESS) {
        if(!is_dup) {
            printf("Failed to register %s, rc:%d\n", obj->rbus_obj.name, rc);
        }
        amxb_rbus_free_obj(obj);
        return rc;
    }
    amxc_llist_append(&amxb_rbus_ctx->registered_objs, &obj->it);

    return rc;
}

static void amxb_rbus_register_functions(amxd_object_t* object,
                                         amxb_rbus_t* amxb_rbus_ctx) {

    char* path = NULL;
    int path_len = 0;

    path = amxd_object_get_path(object,
                                AMXD_OBJECT_SUPPORTED | AMXD_OBJECT_TERMINATE);
    path_len = strlen(path);

    amxc_llist_for_each(it, (&object->functions)) {
        int len = 0;
        amxb_rbus_object_t* obj = NULL;
        amxd_function_t* func = amxc_llist_it_get_data(it, amxd_function_t, it);
        const char* func_name = amxd_function_get_name(func);

        if(func_name == NULL || *func_name == 0) {
            printf("Empty function under %s, skipping...\n", path);
            continue;
        }

        obj = (amxb_rbus_object_t*) calloc(1, sizeof(amxb_rbus_object_t));
        //If allocation fails for one of the functions try registering others
        if(obj == NULL) {
            printf("Failed to register %s%s. Allocation failed\n",
                    path,
                    func_name);
            continue;
        }

        obj->amxb_rbus_ctx = amxb_rbus_ctx;
        obj->rbus_obj.type = RBUS_ELEMENT_TYPE_METHOD;
        obj->rbus_obj.cbTable.methodHandler = amxb_rbus_method_handler;
        len = path_len + strlen(func_name) + strlen("()") + 1;
        if((obj->rbus_obj.name = malloc(len)) == NULL) {
            printf("Failed to register %s%s. Allocation failed\n",
                    path,
                    func_name);
            free(obj);
            continue;
        }

        snprintf(obj->rbus_obj.name, len, "%s%s()", path, func_name);
        amx_rbus_regiser_obj(obj, amxb_rbus_ctx);
    }

    free(path);
    return;
}

static void amxb_rbus_register_params(amxd_object_t* object,
                                      amxb_rbus_t* amxb_rbus_ctx) {
    char* path = NULL;
    int path_len = 0;
    amxc_var_t params;

    path = amxd_object_get_path(object,
                                AMXD_OBJECT_SUPPORTED | AMXD_OBJECT_TERMINATE);
    path_len = strlen(path);
    amxc_var_init(&params);
    when_failed(amxd_object_list_params(object,
                                        &params,
                                        amxd_dm_access_private),
                exit);

    amxc_var_for_each(param, &params) {
        int len;
        amxb_rbus_object_t* obj;
        const char* param_name = GET_CHAR(param, NULL);

        if(param_name == NULL || *param_name == 0) {
            printf("Empty parameter under %s, skipping...\n", path);
            continue;
        }

        obj = (amxb_rbus_object_t*) calloc(1, sizeof(amxb_rbus_object_t));
        //If allocation fails for one of the parameters try registering others
        if(obj == NULL) {
            printf("Failed to register %s%s. Allocation failed\n",
                    path,
                    param_name);
            continue;
        }

        obj->amxb_rbus_ctx = amxb_rbus_ctx;
        obj->rbus_obj.type = RBUS_ELEMENT_TYPE_PROPERTY;
        obj->rbus_obj.cbTable.getHandler = amxb_rbus_get_handler;
        obj->rbus_obj.cbTable.setHandler = amxb_rbus_set_handler;
        len = path_len + strlen(param_name) + 1;
        obj->rbus_obj.name = malloc(len);
        snprintf(obj->rbus_obj.name, len, "%s%s", path, param_name);

        //It is possible to register all parameters as a single array by a
        //single transaction which can improve the performance of registration.
        //But in case if registration of at least one parameter will fail - none
        //of parameters will be registered on rbus.
        //To avoid such problems and ease the registration debugging, each
        //parameter is registered separately.
        //In case of performance issues - need to create an array of objects
        //during the tree traversal and add all object at once after the
        //traversal is finished. Also instances should be stored and added
        //separately after objects are registered.
        amx_rbus_regiser_obj(obj, amxb_rbus_ctx);
    }

exit:
    free(path);
    amxc_var_clean(&params);
    return;
}

#define MAX_UINT32_STR_LEN 11
static bool amxb_rbus_check_instance(rbusHandle_t rbus_handle,
                                    char const* path,
                                    int index) {
    rbusRowName_t* rows = NULL;
    bool isPresent = false;
    rbusError_t rc = RBUS_ERROR_SUCCESS;

    rc = rbusTable_getRowNames(rbus_handle, path, &rows);
    if(RBUS_ERROR_SUCCESS == rc)
    {
         rbusRowName_t* row = rows;
         while(row)
         {
             if(row->instNum == index) {
                isPresent=true;
                 break;
             }
             row = row->next;
         }
         rbusTable_freeRowNames(rbus_handle, rows);
    }
    else
    {
         printf ("Failed to get the data. Error : %d\n\r",rc);
    }
    return isPresent;

}

static void amxb_rbus_add_instance(amxd_object_t* obj,
                                   amxb_rbus_t* amxb_rbus_ctx,
                                   bool force_add) {
    int has_name = 0;
    rbusError_t rc = RBUS_ERROR_SUCCESS;
    char idx[MAX_UINT32_STR_LEN];
    char* path = amxd_object_get_path(obj, AMXD_OBJECT_INDEXED);
    snprintf(idx, MAX_UINT32_STR_LEN, "%d", obj->index);
    has_name = obj->name != NULL && *obj->name != 0 && strcmp(obj->name, idx);
    *(strrchr(path, '.') + 1) = '\0';


    if(force_add || !amxb_rbus_check_instance(amxb_rbus_ctx->rbus_handle, path, obj->index)) {
        rc = rbusTable_registerRow(amxb_rbus_ctx->rbus_handle,
                path,
                obj->index,
                has_name ? obj->name : NULL);

        if(rc != RBUS_ERROR_SUCCESS) {
            printf("Failed registering instance for %s, rc: %d\n", path, rc);
        }
    }
    free(path);
}

#define AMX_ADD_FUNC "_add"
static rbusError_t amxb_rbus_add_instance_handler(rbusHandle_t handle,
                                                 char const* tableName,
                                                 char const* aliasName,
                                                 uint32_t* instNum) {
    rbusError_t rc = RBUS_ERROR_SUCCESS;
    amxc_var_t ret;
    amxc_var_t args;

    *instNum = aliasName ? atoi(aliasName) : 0;
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &args, "index", *instNum);
    amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);

    rc = amxb_rbus_invoke_function(tableName, AMX_ADD_FUNC, &args, &ret);
    when_false(rc == RBUS_ERROR_SUCCESS, exit);

    amxc_htable_for_each(it, amxc_var_constcast(amxc_htable_t, &ret)) {
        if(strncmp(amxc_htable_it_get_key(it), "index", strlen("index")) == 0) {
            amxc_var_t* data = amxc_var_from_htable_it(it);

            *instNum = amxc_var_constcast(uint32_t, data);
        }
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return rc;
}

#define AMX_DEL_FUNC "_del"
static rbusError_t amxb_rbus_del_instance_handler(rbusHandle_t handle,
                                                  char const* name) {
    rbusError_t rc = RBUS_ERROR_SUCCESS;
    char* rel_path = NULL;
    char* obj_path = NULL;
    amxc_var_t ret;
    amxc_var_t args;

    amxb_rbus_split_path(name, &obj_path, &rel_path);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &args, "index", atoi(rel_path));
    amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);

    rc = amxb_rbus_invoke_function(obj_path, AMX_DEL_FUNC, &args, &ret);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return rc;
}

static void amxb_rbus_add_template(amxd_object_t* object,
                                   amxb_rbus_t* amxb_rbus_ctx) {
    amxb_rbus_object_t* obj = NULL;

    obj = (amxb_rbus_object_t*) calloc(1, sizeof(amxb_rbus_object_t));
    when_null(obj, exit);
    obj->amxb_rbus_ctx = amxb_rbus_ctx;
    obj->rbus_obj.type = RBUS_ELEMENT_TYPE_TABLE;
    obj->rbus_obj.name = amxd_object_get_path(object,
                             AMXD_OBJECT_SUPPORTED | AMXD_OBJECT_TERMINATE);
    obj->rbus_obj.cbTable.tableAddRowHandler = amxb_rbus_add_instance_handler;
    obj->rbus_obj.cbTable.tableRemoveRowHandler =
        amxb_rbus_del_instance_handler;

    when_false(amx_rbus_regiser_obj(obj, amxb_rbus_ctx) == RBUS_ERROR_SUCCESS,
               exit);

    //this is needed because tables have to registered before params and functions.
    // a function or param registered in a table that is not yet registered will not work
    amxc_llist_for_each(it, (&object->objects)) {
        amxd_object_t * recursive_object = amxc_llist_it_get_data(it, amxd_object_t, it);
        amxb_rbus_add_template(recursive_object,amxb_rbus_ctx);
    }

    amxb_rbus_register_params(object, amxb_rbus_ctx);
    amxb_rbus_register_functions(object, amxb_rbus_ctx);

exit:
    return;
}

static void amxb_rbus_register_object(amxd_object_t* const object,
                                      AMXB_UNUSED int32_t depth,
                                      void* priv) {
    amxb_rbus_t* amxb_rbus_ctx = (amxb_rbus_t*) priv;
    amxd_object_type_t type = amxd_object_get_type(object);

    when_true((type == amxd_object_root || type == amxd_object_invalid), exit);

    switch(type) {
    case amxd_object_template: {
        amxb_rbus_add_template(object, amxb_rbus_ctx);
    }
    break;
    case amxd_object_singleton: {
        amxb_rbus_register_params(object, amxb_rbus_ctx);
        amxb_rbus_register_functions(object, amxb_rbus_ctx);
        return;
    }
    break;
    case amxd_object_instance: {
        amxb_rbus_add_instance(object, amxb_rbus_ctx, true);
    }
    break;
    default: {
        //TODO: Implement MIB handling
        char *path = amxd_object_get_path(object,
                         AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);

        printf("Object type %d is not implemented, can't register %s\n",
               type,
               path);
        free(path);
    }
    break;
    }

exit:
    return;
}

static bool amxb_rbus_filter_object(amxd_object_t* const object,
                                    AMXB_UNUSED int32_t depth,
                                    AMXB_UNUSED void* priv) {
    bool retval = true;
    amxd_object_t* parent = NULL;
    when_true(amxd_object_get_type(object) == amxd_object_root, exit);

    if(amxd_object_is_attr_set(object, amxd_oattr_private)) {
        retval = false;
        goto exit;
    }

    parent = amxd_object_get_parent(object);
    if(amxd_object_get_type(object) != amxd_object_instance) {
        if(amxd_object_get_type(parent) == amxd_object_template) {
            retval = false;
            goto exit;
        }
    }

exit:
    return retval;
}

static void amxb_rbus_register_tree(AMXB_UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    void* const priv) {
    amxb_rbus_t* amxb_rbus_ctx = (amxb_rbus_t*) priv;
    amxd_object_t* object = amxd_dm_signal_get_object(amxb_rbus_ctx->dm, data);

    if(object != NULL) {
        amxb_rbus_register_object(object, 0, amxb_rbus_ctx);
        amxd_object_hierarchy_walk(object,
                                   amxd_direction_down,
                                   amxb_rbus_filter_object,
                                   amxb_rbus_register_object,
                                   INT32_MAX,
                                   amxb_rbus_ctx);
    }
}

static void amxb_rbus_register_instance(const char* const sig_name,
                                        const amxc_var_t* const data,
                                        void* const priv) {
    amxb_rbus_t* amxb_rbus_ctx = (amxb_rbus_t*) priv;
    amxd_object_t* object = amxd_dm_signal_get_object(amxb_rbus_ctx->dm, data);
    uint32_t index = GET_UINT32(data, "index");

    object = amxd_object_get_instance(object, NULL, index);

    if(object != NULL) {
        amxb_rbus_add_instance(object, priv, false);
    }
}

static void amxb_rbus_unregister_instance(const char* const sig_name,
                                          const amxc_var_t* const data,
                                          void* const priv) {
    uint32_t index = 0;
    rbusError_t rc = RBUS_ERROR_SUCCESS;
    char full_path[MAX_PATH_LEN];
    const char* object_path = GET_CHAR(data, "path");
    amxb_rbus_t* amxb_rbus_ctx = (amxb_rbus_t*) priv;

    index = amxc_var_constcast(uint32_t, amxc_var_get_key(data, "index", 0));
    snprintf(full_path, MAX_PATH_LEN, "%s%d", object_path, index);

    if(amxb_rbus_check_instance(amxb_rbus_ctx->rbus_handle, object_path, index)) {
        rc = rbusTable_unregisterRow(amxb_rbus_ctx->rbus_handle, full_path);
        if(rc != RBUS_ERROR_SUCCESS) {
            printf("Failed unregistering instance for %s, rc: %d\n", full_path, rc);
        }
    }
}
static void amxb_rbus_register_remove(const char* const sig_name,
                                      const amxc_var_t* const data,
                                      void* const priv) {
    const char* obj_path = GET_CHAR(data, "path");
    int len = strlen(obj_path);
    amxb_rbus_t* amxb_rbus_ctx = (amxb_rbus_t*) priv;

    amxc_llist_for_each(it, (&amxb_rbus_ctx->registered_objs)) {
        amxb_rbus_object_t* obj = amxc_llist_it_get_data(it,
                                                         amxb_rbus_object_t,
                                                         it);

        //Unregistering all child objects as well
        if(strncmp(obj->rbus_obj.name, obj_path, len) == 0) {
            rbusError_t rc;
            rc = rbus_unregDataElements(amxb_rbus_ctx->rbus_handle,
                                        1,
                                        &obj->rbus_obj);

            //Not freeing object when unregistration failed, to avoid undefined
            //behavior. Anyway it could lead to dangling objects.
            if(rc != RBUS_ERROR_SUCCESS) {
                printf("Failed to unregister: %s\n", obj->rbus_obj.name);
                continue;
            }
        }
    }
}

static void amxb_rbus_register_dm(UNUSED const char* const sig_name,
                                  UNUSED const amxc_var_t* const data,
                                  void* const priv) {
    amxb_rbus_t* amxb_rbus_ctx = (amxb_rbus_t*) priv;
    amxd_dm_t* dm = amxb_rbus_ctx->dm;

    amxd_object_hierarchy_walk(amxd_dm_get_root(dm),
                               amxd_direction_down,
                               amxb_rbus_filter_object,
                               amxb_rbus_register_object,
                               INT32_MAX,
                               amxb_rbus_ctx);

    amxp_slot_disconnect_with_priv(&dm->sigmngr,
                                   amxb_rbus_register_dm,
                                   amxb_rbus_ctx);

    amxp_slot_connect(&dm->sigmngr,
                      "dm:root-added",
                      NULL,
                      amxb_rbus_register_tree,
                      amxb_rbus_ctx);

    amxp_slot_connect(&dm->sigmngr,
                      "dm:root-removed",
                      NULL,
                      amxb_rbus_register_remove,
                      amxb_rbus_ctx);
    amxp_slot_connect(&dm->sigmngr,
                      "dm:instance-added",
                      NULL,
                      amxb_rbus_register_instance,
                      amxb_rbus_ctx);
    amxp_slot_connect(&dm->sigmngr,
                      "dm:instance-removed",
                      NULL,
                      amxb_rbus_unregister_instance,
                      amxb_rbus_ctx);
    //TODO: Check if following signals needed in rbus case:
    //dm:object-added, dm:object-removed
}

int PRIVATE amxb_rbus_register(void* const ctx,
                               amxd_dm_t* const dm) {
    int status = -1;
    amxb_rbus_t* amxb_rbus_ctx = (amxb_rbus_t*) ctx;
    const amxc_var_t* cfg_ros =
        amxb_rbus_get_config_option("register-on-start-event");

    when_not_null(amxb_rbus_ctx->dm, exit);
    amxb_rbus_ctx->dm = dm;
    g_amxb_rbus_ctx = amxb_rbus_ctx;

    if(amxc_var_dyncast(bool, cfg_ros)) {
        amxp_slot_connect(&dm->sigmngr,
                          "app:start",
                          NULL,
                          amxb_rbus_register_dm,
                          amxb_rbus_ctx);
    } else {
        amxb_rbus_register_dm(NULL, NULL, ctx);
    }

    status = 0;

exit:
    return status;
}
