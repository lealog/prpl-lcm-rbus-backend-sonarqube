/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#if !defined(__AMXB_RBUS_H__)
#define __AMXB_RBUS_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_function.h>

#include <amxb/amxb.h>
#include <amxb/amxb_be_intf.h>

#include <rbus.h>

#define DEFAULT_TIMEOUT 5
#define DEFAULT_DEPTH 1
typedef struct _amxb_rbus {
    rbusHandle_t rbus_handle;
    int dummy_fd;
    amxc_llist_t registered_objs;
    amxd_dm_t* dm;
} amxb_rbus_t;

typedef struct _amxb_rbus_object {
    amxc_llist_it_t it;
    rbusDataElement_t rbus_obj;
    amxb_rbus_t* amxb_rbus_ctx;
} amxb_rbus_object_t;

#define TODO_PRINT fprintf(stderr, "TODO: implement %s\n", __func__)

amxb_be_info_t* amxb_be_info(void);

void* PRIVATE amxb_rbus_connect(const char* host,
                                const char* port,
                                const char* path,
                                amxp_signal_mngr_t* sigmngr);

int PRIVATE amxb_rbus_disconnect(void* ctx);

int PRIVATE amxb_rbus_get_fd(void* ctx);

int PRIVATE amxb_rbus_read(void* ctx);

void PRIVATE amxb_rbus_free(void* ctx);

int PRIVATE amxb_rbus_invoke(void* const ctx,
                             amxb_invoke_t* invoke_ctx,
                             amxc_var_t* args,
                             amxb_request_t* request,
                             int timeout);

int PRIVATE amxb_rbus_async_invoke(void* const ctx,
                                   amxb_invoke_t* invoke_ctx,
                                   amxc_var_t* args,
                                   amxb_request_t* request);

int PRIVATE amxb_rbus_wait_request(void* const ctx,
                                   amxb_request_t* request,
                                   int timeout);

int PRIVATE amxb_rbus_close_request(void* const ctx,
                                    amxb_request_t* request);

int PRIVATE amxb_rbus_subscribe(void* const ctx,
                                const char* object);

int PRIVATE amxb_rbus_unsubscribe(void* const ctx,
                                  const char* object);

void PRIVATE amxb_rbus_obj_it_free(amxc_llist_it_t* it);

int PRIVATE amxb_rbus_register(void* const ctx,
                               amxd_dm_t* const dm);

int PRIVATE amxb_rbus_list(void* const bus_ctx,
                           const char* object,
                           uint32_t flags,
                           uint32_t access,
                           amxb_request_t* request);

const amxc_var_t* amxb_rbus_get_config_option(const char* name);
int amxb_rbus_resolve(amxb_rbus_t* ctx,
                     const char* object_path,
                     const char* rel_path,
                     int32_t depth,
                     uint32_t flags,
                     bool* key_path,
                     amxc_var_t* resolved_objects);

int PRIVATE amxb_rbus_get(void* const ctx,
                               const char* object,
                               const char* search_path,
                               int32_t depth,
                               uint32_t access,
                               amxc_var_t* ret,
                               int timeout);
int PRIVATE amxb_rbus_set (void* const ctx,
                   const char* object,
                   const char* search_path,
                   uint32_t flags,
                   amxc_var_t* values,
                   amxc_var_t* ovalues,
                   uint32_t access,
                   amxc_var_t* ret,
                   int timeout);
int client_rbus_set(amxb_rbus_t* ctx,
                   const char* object,
                   amxc_var_t* values);
const char *getDataTypetoString(int  type);
rbusValueType_t getDataType_fromString(const char* pType);

bool PRIVATE amxb_rbus_has(void* ctx,
                const char* object);

void PRIVATE amxb_rbus_set_value(amxc_var_t* params,
                                rbusValue_t value,
                                const char *name);

void PRIVATE amxb_rbus_set_retval(rbusValue_t value,
                                 amxc_var_t* data,
                                 char* param);

#ifdef __cplusplus
}
#endif

#endif // __AMXB_RBUS_H__
