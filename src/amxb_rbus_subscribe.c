/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#include <stdlib.h>

#include "amxb_rbus.h"

int PRIVATE amxb_rbus_subscribe(void* const ctx,
                                const char* object) {
    int ret = 0;

    // TODO: implement function
    TODO_PRINT;
    return ret;
}

int PRIVATE amxb_rbus_unsubscribe(void* const ctx,
                                  const char* object) {
    int ret = 0;

    // TODO: implement function
    TODO_PRINT;
    return ret;
}

