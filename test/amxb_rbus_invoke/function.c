
#include <stdlib.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc_variant_type.h>
#include <amxc/amxc_lqueue.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_types.h>

amxd_status_t _getResult(UNUSED amxd_object_t* obj,UNUSED amxd_function_t* func,amxc_var_t* args,UNUSED amxc_var_t* ret);
amxd_status_t _getResult(UNUSED amxd_object_t* obj,UNUSED amxd_function_t* func,UNUSED amxc_var_t* args,UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

amxd_status_t _setPeriodicInform(UNUSED amxd_object_t* obj,UNUSED amxd_function_t* func,amxc_var_t* args,UNUSED amxc_var_t* ret);
amxd_status_t _setPeriodicInform(UNUSED amxd_object_t* obj,UNUSED amxd_function_t* func,UNUSED amxc_var_t* args,UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}
