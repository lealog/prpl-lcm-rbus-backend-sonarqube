/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#define _GNU_SOURCE
#include "amxb_rbus.h"
#include <amxb_rbus_version.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <string.h>

#define MAX_COMPONENT_NAME 50

static const amxc_var_t* config_opts = NULL;

static int amxb_rbus_set_config(amxc_var_t* const configuration) {
    config_opts = configuration;

    // TODO: implement function
    TODO_PRINT;
    return 0;
}

const amxc_var_t* amxb_rbus_get_config_option(const char* name) {
    // TODO: implement function
    TODO_PRINT;
    return GET_ARG(config_opts, name);
}

void* PRIVATE amxb_rbus_connect(const char* host,
                                const char* port,
                                const char* path,
                                amxp_signal_mngr_t* sigmngr) {
    // TODO: Add sigmngr, subscribers, list initialization when usage will be
    // added
    int flags;
    rbusError_t rc;
    amxb_rbus_t* amxb_rbus_ctx = NULL;
    char comp_name[50] = "\0"; //For unique component name for each process 

    snprintf(comp_name,MAX_COMPONENT_NAME,"%s_%d", "amx_rbus_ba", getpid()); //For unique component name for each process

    when_not_null(host, exit);
    when_not_null(port, exit);

    amxb_rbus_ctx = (amxb_rbus_t*) calloc(1, sizeof(amxb_rbus_t));
    when_null(amxb_rbus_ctx, exit);

    rc = rbus_open(&amxb_rbus_ctx->rbus_handle, comp_name);

    if(rc != RBUS_ERROR_SUCCESS || amxb_rbus_ctx->rbus_handle == NULL) {
        free(amxb_rbus_ctx);
        amxb_rbus_ctx = NULL;
        goto exit;
    }

    amxb_rbus_ctx->dummy_fd = socket(AF_UNIX, SOCK_DGRAM, 0);

    if(amxb_rbus_ctx->dummy_fd < 0) {
        free(amxb_rbus_ctx);
        amxb_rbus_ctx = NULL;
        goto exit;
    }

    flags = fcntl(amxb_rbus_ctx->dummy_fd, F_GETFL, 0) | O_NONBLOCK | O_CLOEXEC;
    fcntl(amxb_rbus_ctx->dummy_fd, F_SETFL, flags);

    amxc_llist_init(&amxb_rbus_ctx->registered_objs);

exit:
    return amxb_rbus_ctx;
}

int PRIVATE amxb_rbus_disconnect(void* ctx) {
    int retval = 0;
    amxb_rbus_t* amxb_rbus_ctx = ctx;

    when_null(amxb_rbus_ctx, exit);
    if(amxb_rbus_ctx->dummy_fd != -1) {
        retval = close(amxb_rbus_ctx->dummy_fd);
    }
    if(rbus_close(amxb_rbus_ctx->rbus_handle) != RBUS_ERROR_SUCCESS) {
        retval = -1;
    }

exit:
    return retval;
}

// fd which is actually used for rbus communication is created by rtmessage lib
// which is the part of rbus. This fd is encapsulated inside rtmessage and not
// exposed outside. ubus is single-threaded while rbus is multi-threaded. That's
// why ubus exposes fd, while in rbus it won't be possible as everything is
// handled by a separate thread.
// Dummy fd is created to avoid fails when ambiorix clients polling this fd
// (such action is needed for ubus backend).
int PRIVATE amxb_rbus_get_fd(void* ctx) {
    return ((amxb_rbus_t*) ctx)->dummy_fd;
}

// Reading of the proper fd is done by rtmessage lib which is the part of rbus.
// No need to specifically read something here
int PRIVATE amxb_rbus_read(void* ctx) {
    return 0;
}

void PRIVATE amxb_rbus_free(void* ctx) {
    if (!ctx) return;
    amxb_rbus_t* amxb_rbus_ctx = ctx;

    amxc_llist_clean(&amxb_rbus_ctx->registered_objs, amxb_rbus_obj_it_free);
    free(amxb_rbus_ctx);
}

static int amxb_rbus_wait_for(void* const ctx, UNUSED const char* object) {
    int retval = 0;

    // TODO: implement function
    TODO_PRINT;
    return retval;
}

static uint32_t amxb_rbus_capabilities(UNUSED void* ctx) {
    // TODO: implement function
    TODO_PRINT;
    return AMXB_BE_DISCOVER;
}

bool amxb_rbus_has(void* ctx, const char* object) {
    bool has_object = false;
    amxb_rbus_t* rbus_ctx = (amxb_rbus_t*)(ctx);
    int numOfOutVals = 0;
    rbusProperty_t outputVals = NULL;
    const char *pInputParam[2] = {object, NULL};
    rbusError_t error_code;

    if (object == NULL) {
        return false; 
    }

    error_code = rbus_getExt(rbus_ctx->rbus_handle, 1, pInputParam, &numOfOutVals, &outputVals);
    // If the error code is success then it means the item exists on the bus.
    // If the item does not exist on the bus this will return RBUS_ERROR_DESTINATION_NOT_REACHABLE
    // RBUS_ERROR_INVALID_OPERATION depending on the rbus type (although this does indicate it does exist)
    // or RBUS_ERROR_ACCESS_NOT_ALLOWED if the item is a table item that doesn't exist
    // with anything else returned other than this indicating some sort of unknown condition
    if (error_code == RBUS_ERROR_SUCCESS || error_code == RBUS_ERROR_INVALID_OPERATION) {
        has_object = true;
    } else if (error_code == RBUS_ERROR_DESTINATION_NOT_REACHABLE || error_code == RBUS_ERROR_ACCESS_NOT_ALLOWED) {
        //check if the object exists if parsed as a wildcard 
        //amx may look for top level components, which may not themselves be elements.
        //this includes tables, so checking for "example" or "example.table" will not work, but checking for the same with an appended '.' will.
        if (outputVals != NULL ) {
            rbusProperty_Release(outputVals); 
        }
        const size_t wildcard_len = strlen(object) +2;
        char * wildcard = (char*)malloc(wildcard_len);
        if (wildcard == NULL ) { goto error; }

        
        if (snprintf(wildcard,wildcard_len,"%s.",object) != (int)wildcard_len-1) {
            free(wildcard);
            goto error; 
        }

        pInputParam[0] = wildcard;
        error_code = rbus_getExt(rbus_ctx->rbus_handle, 1, pInputParam, &numOfOutVals, &outputVals);
        free(wildcard);

        if (error_code == RBUS_ERROR_SUCCESS) {
            has_object = true;
        }

    } else {
        goto error;
    }

    if (outputVals != NULL ) {
        rbusProperty_Release(outputVals); 
    }
    return has_object;

    error:
    fprintf(stderr,"Unknown error during call to has(%s) rbus error=%i\n",object,error_code);
    return false;
}

static amxb_be_funcs_t amxb_rbus_impl = {
    .it = { .ait = NULL, .key = NULL, .next = NULL },
    .handle = NULL,
    .connections = { .head = NULL, .tail = NULL },
    .name = "rbus",
    .size = sizeof(amxb_be_funcs_t),
    .connect = amxb_rbus_connect,
    .disconnect = amxb_rbus_disconnect,
    .get_fd = amxb_rbus_get_fd,
    .read = amxb_rbus_read,
    .new_invoke = NULL,
    .free_invoke = NULL,
    .invoke = amxb_rbus_invoke,
    .async_invoke = amxb_rbus_async_invoke,
    .close_request = amxb_rbus_close_request,
    .wait_request = amxb_rbus_wait_request,
    .subscribe = amxb_rbus_subscribe,
    .unsubscribe = amxb_rbus_unsubscribe,
    .free = amxb_rbus_free,
    .register_dm = amxb_rbus_register,
    .get = amxb_rbus_get,
    .set = amxb_rbus_set,
    .add = NULL,
    .del = NULL,
    .get_supported = NULL,
    .set_config = amxb_rbus_set_config,
    .describe = NULL,
    .list = amxb_rbus_list,
    .listen = NULL,
    .accept = NULL,
    .read_raw = NULL,
    .wait_for = amxb_rbus_wait_for,
    .capabilities = amxb_rbus_capabilities,
    .has = amxb_rbus_has,
};

/* TODO: Replace with proper versions */
static amxb_version_t sup_min_lib_version = {
    .major = 0,
    .minor = 0,
    .build = 0
};

/* TODO: Replace with proper versions */
static amxb_version_t sup_max_lib_version = {
    .major = 9,
    .minor = 9,
    .build = 9
};

/* TODO: Check proper versions */
static amxb_version_t rbus_be_version = {
    .major = AMXB_RBUS_VERSION_MAJOR,
    .minor = AMXB_RBUS_VERSION_MINOR,
    .build = AMXB_RBUS_VERSION_BUILD,
};

amxb_be_info_t amxb_rbus_be_info = {
    .min_supported = &sup_min_lib_version,
    .max_supported = &sup_max_lib_version,
    .be_version = &rbus_be_version,
    .name = "rbus",
    .description = "AMXB Backend for RBUS (Openwrt/Prplwrt)",
    .funcs = &amxb_rbus_impl,
};

amxb_be_info_t* amxb_be_info(void) {
    // TODO: implement function
    TODO_PRINT;
    // TODO: Remove debug version print when tag is put and version is set
    printf("Amx rbus backend version: %d:%d:%d\n", rbus_be_version.major,
                                                   rbus_be_version.minor,
                                                   rbus_be_version.build);
    return &amxb_rbus_be_info;
}

void amxb_rbus_set_value(amxc_var_t* params,
                                rbusValue_t value,
                                const char *name) {
    int len;

    switch(rbusValue_GetType(value)) {
    case RBUS_BOOLEAN: {
        amxc_var_add_key(bool, params, name, rbusValue_GetBoolean(value));
    }
    break;
    case RBUS_INT8: {
        amxc_var_add_key(int8_t, params, name, rbusValue_GetInt8(value));
    }
    break;
    case RBUS_UINT8: {
        amxc_var_add_key(uint8_t, params, name, rbusValue_GetUInt8(value));
    }
    break;
    case RBUS_INT16: {
        amxc_var_add_key(int16_t, params, name, rbusValue_GetInt16(value));
    }
    break;
    case RBUS_UINT16: {
        amxc_var_add_key(uint16_t, params, name, rbusValue_GetUInt16(value));
    }
    break;
    case RBUS_INT32: {
        amxc_var_add_key(int32_t, params, name, rbusValue_GetInt32(value));
    }
    break;
    case RBUS_UINT32: {
        amxc_var_add_key(uint32_t, params, name, rbusValue_GetUInt32(value));
    }
    break;
    case RBUS_INT64: {
        amxc_var_add_key(int64_t, params, name, rbusValue_GetInt64(value));
    }
    break;
    case RBUS_UINT64: {
        amxc_var_add_key(uint64_t, params, name, rbusValue_GetUInt64(value));
    }
    break;
    case RBUS_DOUBLE: {
        amxc_var_add_key(double, params, name, rbusValue_GetDouble(value));
    }
    break;
    case RBUS_STRING: {
        amxc_var_add_key(cstring_t, params, name, rbusValue_GetString(value,
                                                                      &len));
    }
    case RBUS_NONE: {
        //do nothing, RBUS_NONE parameters should not be passed as input args and ignored
    }
    break;
    default: {
        fprintf(stderr,"unhandled rbus type %i for %s\n",rbusValue_GetType(value),name);
    }
    }
}

void amxb_rbus_set_retval(rbusValue_t value,
                                 amxc_var_t* data,
                                 char* param) {

    switch(amxc_var_type_of(data)) {
    case AMXC_VAR_ID_BOOL: {
        rbusValue_SetBoolean(value, amxc_var_constcast(bool, data));
    }
    break;
    case AMXC_VAR_ID_INT8: {
        rbusValue_SetInt8(value, amxc_var_constcast(int8_t, data));
    }
    break;
    case AMXC_VAR_ID_UINT8: {
        rbusValue_SetUInt8(value, amxc_var_constcast(uint8_t, data));
    }
    break;
    case AMXC_VAR_ID_INT16: {
        rbusValue_SetInt16(value, amxc_var_constcast(int16_t, data));
    }
    break;
    case AMXC_VAR_ID_UINT16: {
        rbusValue_SetUInt16(value, amxc_var_constcast(uint16_t, data));
    }
    break;
    case AMXC_VAR_ID_INT32: {
        rbusValue_SetInt32(value, amxc_var_constcast(int32_t, data));
    }
    break;
    case AMXC_VAR_ID_UINT32: {
        rbusValue_SetUInt32(value, amxc_var_constcast(uint32_t, data));
    }
    break;
    case AMXC_VAR_ID_INT64: {
        rbusValue_SetInt64(value, amxc_var_constcast(int64_t, data));
    }
    break;
    case AMXC_VAR_ID_UINT64: {
        rbusValue_SetUInt64(value, amxc_var_constcast(uint64_t, data));
    }
    break;
    case AMXC_VAR_ID_DOUBLE: {
        rbusValue_SetDouble(value, amxc_var_constcast(double, data));
    }
    break;
    case AMXC_VAR_ID_HTABLE: {
        const amxc_htable_t* table = amxc_var_constcast(amxc_htable_t, data);

        amxc_htable_for_each(it, table) {
            const char* key = amxc_htable_it_get_key(it);
            amxc_var_t* val = amxc_var_from_htable_it(it);

            if(!strcmp(key, param)){
                amxb_rbus_set_retval(value, val, param);
                break;
            }
        }
    }
    break;
    case AMXC_VAR_ID_CSTRING: {
        char* string = amxc_var_dyncast(cstring_t, data);

        rbusValue_SetString(value, string);
        free(string);
    }
    break;
    case AMXC_VAR_ID_TIMESTAMP: {
        rbusDateTime_t tv;
        //TODO convert from amxc timestamp to rbus datetime
        rbusValue_SetTime(value,&tv);
    }
    break;
    case AMXC_VAR_ID_NULL: {
        //fprintf(stderr,"null type for %s %i\n" ,param,amxc_var_type_of(data));
        //do nothing
    }
    break;
    default: {
        fprintf(stderr,"unhandled amxc type with id %i\n" ,amxc_var_type_of(data));
    break;
    }
    }
}
