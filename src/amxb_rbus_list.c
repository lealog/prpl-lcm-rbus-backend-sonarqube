/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <string.h>

#include "amxb_rbus.h"

#define NO_ELEMENTS 0

int amxb_rbus_resolve_list(amxb_rbus_t* ctx,
                     const char* object_path,
                     bool  nextLevel,
                     amxb_request_t* request );

int amxb_rbus_resolve_list(amxb_rbus_t* ctx,
                     const char* object_path,
		     bool  nextLevel,
                     amxb_request_t* request ) {
    
    int retval = AMXB_ERROR_INTERNAL;
    int numElements = 0;
    char** pElementNames = NULL;

    retval = rbus_discoverComponentDataElements (ctx->rbus_handle, object_path, nextLevel, &numElements, &pElementNames);
    if(RBUS_ERROR_SUCCESS == retval)
    {
        
        if(numElements)
        {
            request->bus_retval = numElements;
            request->bus_data = pElementNames;
        }
        else
        {
            request->bus_retval = NO_ELEMENTS;
            free(pElementNames);
        }
    }
    else
    {
        fprintf (stderr,"Failed to discover elements. Error Code = %d\n\r", retval);
    }

    return retval;
}

int amxb_rbus_list(void* const ctx,
                   const char* object,
                   uint32_t flags,
                   uint32_t access,
                   amxb_request_t* request) {
	
    int retval = -1;
    amxb_rbus_t* amxb_rbus_ctx = (amxb_rbus_t*) ctx;
    const amxb_bus_ctx_t* bus_ctx = (amxb_bus_ctx_t*) ctx;  
    if (!object) {
        amxb_close_request(&request);
        goto exit;
    }
    retval = amxb_rbus_resolve_list(amxb_rbus_ctx, object, access, request);
   
    if(retval == AMXB_STATUS_OK)
    {
        if (request && request->cb_fn ) request->cb_fn(bus_ctx, NULL, request);
    }
    else
    {
        fprintf(stderr,"Failed with Return Value: %d\n", retval);
    }

exit:    

    return retval;   
}

