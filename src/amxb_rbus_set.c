/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <string.h>

#include "amxb_rbus.h"
#include <amxb_rbus_version.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_path.h>

#define RBUS_CLI_MAX_PARAM      25
#define RBUS_CLI_SESSION_ID     4230

const char *getDataTypetoString(int  type){
    const char* pTextData = "None";

    switch(type)
    {
        case AMXC_VAR_ID_CSTRING:
             pTextData ="string";
             break;
        case AMXC_VAR_ID_INT8:
             pTextData ="int8";
             break;
        case AMXC_VAR_ID_INT16:
             pTextData ="int16";
             break;
        case AMXC_VAR_ID_INT32:
             pTextData ="int32";
             break;
        case AMXC_VAR_ID_INT64:
             pTextData ="int64";
             break;
        case AMXC_VAR_ID_UINT8:
             pTextData ="uint8";
             break;
        case AMXC_VAR_ID_UINT16:
             pTextData ="uint16";
             break;
        case AMXC_VAR_ID_UINT32:
             pTextData = "uint32";
             break;
        case AMXC_VAR_ID_UINT64:
             pTextData = "uint64";
             break;
        case AMXC_VAR_ID_FLOAT:
             pTextData = "float";
             break;
        case AMXC_VAR_ID_DOUBLE:
             pTextData = "double";
             break;
        case AMXC_VAR_ID_BOOL:
             pTextData = "boolean";
             break;
        case AMXC_VAR_ID_NULL:
             pTextData = "unknown";
             break;
    }
    return pTextData ;
}
rbusValueType_t getDataType_fromString(const char* pType){
    rbusValueType_t rc = RBUS_NONE;

    if (strncasecmp ("boolean", pType, 4) == 0)
        rc = RBUS_BOOLEAN;
    else if (strncasecmp("char", pType, 4) == 0)
        rc = RBUS_CHAR;
    else if (strncasecmp("byte", pType, 4) == 0)
        rc = RBUS_BYTE;
    else if (strncasecmp("int8", pType, 4) == 0)
        rc = RBUS_INT8;
    else if (strncasecmp("uint8", pType, 5) == 0)
        rc = RBUS_UINT8;
    else if (strncasecmp("int16", pType, 5) == 0)
        rc = RBUS_INT16;
    else if (strncasecmp("uint16", pType, 6) == 0)
        rc = RBUS_UINT16;
    else if (strncasecmp("int32", pType, 5) == 0)
        rc = RBUS_INT32;
    else if (strncasecmp("uint32", pType, 6) == 0)
        rc = RBUS_UINT32;
    else if (strncasecmp("int64", pType, 5) == 0)
        rc = RBUS_INT64;
    else if (strncasecmp("uint64", pType, 6) == 0)
        rc = RBUS_UINT64;
    else if (strncasecmp("single", pType, 5) == 0)
        rc = RBUS_SINGLE;
    else if (strncasecmp("double", pType, 6) == 0)
        rc = RBUS_DOUBLE;
    else if (strncasecmp("datetime", pType, 4) == 0)
        rc = RBUS_DATETIME;
    else if (strncasecmp("string", pType, 6) == 0)
        rc = RBUS_STRING;
    else if (strncasecmp("bytes", pType, 3) == 0)
        rc = RBUS_BYTES;
    /* Risk handling, if the user types just int, lets consider int32; same for unsigned too  */
    else if (strncasecmp("int", pType, 3) == 0)
        rc = RBUS_INT32;
    else if (strncasecmp("uint", pType, 4) == 0)
        rc = RBUS_UINT32;

    return rc;
}

int client_rbus_set(amxb_rbus_t* ctx,
                   const char* object,
                   amxc_var_t* values) {
    rbusError_t rc = RBUS_ERROR_SUCCESS;
    bool isCommit = true;
    int sessionId = 0;
    rbusValue_t setVal;
    bool value = false;
    char* full_path= NULL;
    char* data=NULL;
    int ptype=0;
        
    if(values != NULL){
        when_true(amxc_var_type_of(values) != AMXC_VAR_ID_HTABLE, exit);
    
        const amxc_htable_t *var_table = amxc_var_constcast(amxc_htable_t, values);
        
        amxc_htable_for_each(it,var_table) {

            //free previous loop of htable it (only last one is used?)
            free(full_path);
            free(data);
            const char* key = amxc_htable_it_get_key(it);
            amxc_var_t* val = amxc_var_from_htable_it(it);
            char *str_value = amxc_var_dyncast(cstring_t, val);
            
            ptype=amxc_var_type_of(val);

            const size_t full_path_size = strnlen(object,RBUS_CLI_MAX_PARAM) + strnlen(key,RBUS_CLI_MAX_PARAM);
            full_path = malloc(full_path_size + 1);
            if (!full_path) {
                goto exit;
            }
            snprintf(full_path,full_path_size+1,"%s%s",object,key);
            
           
            data=strndup(str_value,strnlen(str_value,RBUS_CLI_MAX_PARAM));
            free(str_value);
        }
            
     }
    /* Get Param Type */
    rbusValueType_t type = getDataType_fromString(getDataTypetoString(ptype));
    rbusValue_Init(&setVal);
    value = rbusValue_SetFromString(setVal, type, data);

    if(value == false){
            rc = RBUS_ERROR_INVALID_INPUT;
            /* Free the data pointer that was allocated */
            rbusValue_Release(setVal);
            printf ("Invalid data value passed to set.%d\n", rc);
            goto exit;
    }
    if (type != RBUS_NONE){

            /* For non-interactive mode, regardless of COMMIT value tha is passed, we assume the isCommit as TRUE */
            isCommit = true;
            sessionId = 0;

            rbusSetOptions_t opts = {isCommit,sessionId};

            rc = rbus_set(ctx->rbus_handle, full_path, setVal, &opts);

            /* Free the data pointer that was allocated */
            rbusValue_Release(setVal);
    }
    else
    {
            rc = RBUS_ERROR_INVALID_INPUT;
            printf ("Invalid data type. Please see the help\n\r");
    }
    
    if(RBUS_ERROR_SUCCESS == rc)
    {
        printf ("setvalues succeeded..\n\r");
    }
    else
    {
        printf ("setvalues failed with return value: %d\n\r", rc);
    }

exit:
    free(full_path);
    free(data);
    return rc;
}

int PRIVATE  amxb_rbus_set (void* const ctx,
                   const char* object,
                   AMXB_UNUSED const char* search_path,
                   AMXB_UNUSED uint32_t flags,
                   amxc_var_t* values,
                   AMXB_UNUSED amxc_var_t* ovalues,
                   AMXB_UNUSED uint32_t access,
                   AMXB_UNUSED  amxc_var_t* ret,
                   AMXB_UNUSED int timeout){
    int retval = -1;
	
    amxb_rbus_t* amxb_rbus_ctx = (amxb_rbus_t*) ctx;
    amxc_var_t* resolved_table=NULL;
    
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
	
    retval=client_rbus_set(amxb_rbus_ctx,object,values);
	
    if(retval == -1) {
        amxc_var_clean(ret);
        printf("Remote is not avilable\n");

    } 
    else if(retval == 0) {
        free(resolved_table);
    }
    if(retval != 0) {
        amxc_var_clean(ret);
        retval = AMXB_ERROR_BUS_NOT_FOUND;
        goto exit;
    }

exit:

    return retval;
}

