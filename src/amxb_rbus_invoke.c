/* SPDX-License-Identifier: BSD-2-Clause-Patent
 *
 * SPDX-FileCopyrightText: 2022 the amxb_rbus contributors (see AUTHORS.md)
 *
 * This code is subject to the terms of the BSD+Patent license.
 * See LICENSE file for more details.
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "amxb_rbus.h"
#include <stdlib.h>
#include <string.h>



// Read through amx hash table values (where function args are stored)
// and convert them to rbus properties in an rbus object
static void convert_amxc_htable_to_rbus_obj(amxc_htable_t * htable,
                                            rbusObject_t params ) {
    rbusValue_t value = NULL;
    rbusProperty_t prop = NULL;
    if (!htable || htable->items == 0 ) { return; }
    amxc_htable_iterate(it, (htable)) {
        const char *key = amxc_htable_it_get_key(it);
        amxc_var_t * var = amxc_var_from_htable_it(it);
        if (var != NULL) {
            rbusValue_Init(&value);
            amxb_rbus_set_retval(value,var,NULL);
            rbusProperty_Init(&prop, key, value);
            rbusObject_SetProperty(params, prop);

            rbusValue_Release(value);
            rbusProperty_Release(prop);   
        }
    }
}

// common setup for both invoke and invoke async
// returns 0 on success -1 on error
// path , args, and in_params must be freed by the caller
static int rbus_invoke_setup(char * object,
                            char * method,
                            rbusObject_t  * in_params,
                            amxc_var_t ** args,
char ** path) {

    const size_t path_len = strlen(object) + strlen(method) + sizeof(".()");
    *path = malloc(path_len);

    //failure to do either of these mean something has gone wrong so exit early
    if (*path == NULL || ((size_t)snprintf(*path,path_len,"%s.%s()",object,method) != path_len -1) ) {
        fprintf(stderr,"could not set path\n");
        return -1; 
    }

    rbusObject_Init(in_params,NULL);
    if (args !=NULL && *args != NULL ) { convert_amxc_htable_to_rbus_obj(&(*args)->data.vm,*in_params); }

    return 0;
}

int PRIVATE amxb_rbus_invoke(void* const ctx,
                             amxb_invoke_t* invoke_ctx,
                             amxc_var_t* args,
                             amxb_request_t* request,
                             UNUSED int timeout) {
    int ret = -1;
    rbusObject_t in_params = NULL, out_params = NULL;
    rbusProperty_t prop = NULL;
    char * path = NULL;
    amxc_var_t* item = NULL;
    request->result = NULL;
    request->bus_data = NULL;

    if (rbus_invoke_setup(invoke_ctx->object,invoke_ctx->method,&in_params,&args,&path) == -1) {
        goto exit;
    }

    if (amxb_rbus_has(ctx,path)) { //if the function doesn't exist calling invoke would result in a null being de-referenced
        ret = rbusMethod_Invoke(((amxb_rbus_t*)ctx)->rbus_handle,path,in_params,&out_params);

        //convert rbus params from the out params back, storing the result in request->result
        amxc_var_new(&request->result);
        amxc_var_set_type(request->result, AMXC_VAR_ID_HTABLE);
        
        prop = rbusObject_GetProperties(out_params);
        while (prop) {
            amxc_var_new(&item);
            amxb_rbus_set_value(item,rbusProperty_GetValue(prop), rbusProperty_GetName(prop));
            amxc_var_set_index(request->result, -1, item, AMXC_VAR_FLAG_DEFAULT);  
            prop = rbusProperty_GetNext(prop);
            amxc_var_delete(&item);
        }

    } else { 
        fprintf(stderr,"tried to invoke %s, but it doesn't exist on rbus\n",path);
    }
    

    rbusObject_Release(out_params);

    if(request->cb_fn != NULL) {
        request->cb_fn(ctx, request->result, request->priv);
    }

    if(request->done_fn != NULL) {
        request->done_fn(ctx, request, ret, request->priv);
    } else {
        if (request && request->result) amxc_var_delete(&request->result);
    }
    exit:
    request->bus_retval = ret;
    free(path);
    rbusObject_Release(in_params);
    return ret;
}

static void rbus_generic_callback(UNUSED rbusHandle_t handle,
                                    UNUSED const char* method,
                                    UNUSED rbusError_t err,
                                    rbusObject_t params) {
    rbusProperty_t prop = rbusObject_GetProperties(params);
    rbusProperty_t callback = rbusObject_GetProperty(params,"__callbackPtr");
    amxb_request_t* request = NULL;
    const amxb_bus_ctx_t* ctx = NULL;
    amxc_var_t* item = NULL;
    int ret = -1;

    if (callback != NULL) {
        #if INTPTR_MAX == INT64_MAX
        request = (amxb_request_t*) rbusValue_GetUInt64(rbusProperty_GetValue(callback));
        #elif INTPTR_MAX == INT32_MAX
        request = (amxb_request_t*) rbusValue_GetUInt32(rbusProperty_GetValue(callback));
        #endif
        ctx = amxb_request_get_ctx(request);
    }

    if(request !=NULL) {

        while (prop) {
            if (prop != callback) {
                amxc_var_new(&item);
                amxb_rbus_set_value(item,rbusProperty_GetValue(prop), rbusProperty_GetName(prop));
                amxc_var_set_index(request->result, -1, item, AMXC_VAR_FLAG_DEFAULT);
                amxc_var_delete(&item);
            }       
            prop = rbusProperty_GetNext(prop);
        }

        if (request->cb_fn != NULL) {
            request->cb_fn(ctx, request->result, request->priv);
        }

        *((bool*)request->bus_data) = true;
        if(request->done_fn != NULL) {
            request->done_fn(ctx, request, ret, request->priv);
        }
    } else {
        fprintf(stderr,"callback request data not set\n");
    }
    
}

int PRIVATE amxb_rbus_async_invoke(void* const ctx,
                                   amxb_invoke_t* invoke_ctx,
                                   amxc_var_t* args,
                                   amxb_request_t* request) {
    int ret = -1;
    rbusObject_t in_params = NULL;
    // rbusObject_t out_params = NULL;
    rbusProperty_t prop = NULL;
    char * path = NULL;
    if (rbus_invoke_setup(invoke_ctx->object,invoke_ctx->method,&in_params,&args,&path) == -1) {
        goto exit;
    }

    if (amxb_rbus_has(ctx,path)) { //if the function doesn't exist calling invoke would result in a null being de-referenced
        //add the callbackPtr to inParams
        rbusProperty_Init(&prop, "__callbackPtr", NULL);
        rbusProperty_SetUInt64(prop,(uintptr_t)request);
        rbusObject_SetProperty(in_params, prop);
        rbusProperty_Release(prop); 
        ret = rbusMethod_InvokeAsync(((amxb_rbus_t*)ctx)->rbus_handle,path,in_params,rbus_generic_callback,0);
        request->bus_data = malloc(sizeof(bool)); //bus data being used as a flag to indicate that the callback is done
        if (!request->bus_data) {
            goto exit;
        }
        *((bool*)request->bus_data) = false;
    } else { 
        fprintf(stderr,"tried to invoke %s, but it doesn't exist on rbus\n",path);   
    }

    exit:
    rbusObject_Release(in_params);
    request->bus_retval = ret;
    free(path);
    return ret;
}

int PRIVATE amxb_rbus_wait_request(AMXB_UNUSED void* const ctx,
                                   UNUSED amxb_request_t* request,
                                   UNUSED int timeout) {
    int ret = 0;

    int timer = 0;
    const int timeout_in_ms = timeout * 1000; //timeout
    const struct timespec one_ms = {0,1000000L};
    while ( timer < timeout_in_ms) {
        if (!request->bus_data || *((bool*)request->bus_data) == true ) break;
        timer++;
        nanosleep(&one_ms,NULL); //sleep 1ms
    }

    return ret;
}

int PRIVATE amxb_rbus_close_request(UNUSED void* const ctx,
                                    amxb_request_t* request) {
    int ret = 0;
    // callback is in progress, closing the connection without waiting for it 
    // would result in accessing freed memory, so wait for it to finish
    if (request && request->bus_data && *((bool*)request->bus_data) == false ) { 
        amxb_rbus_wait_request(ctx,request,2);
    }
    free(request->bus_data);
    amxc_var_delete(&request->result);
    request->result = NULL;
    request->bus_data = NULL;
    return ret;
}
